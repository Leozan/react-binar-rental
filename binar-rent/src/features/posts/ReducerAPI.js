// A mock function to mimic making an async request for data
// https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json
import axios from "axios";

export function fetchCount(amount = 1) {
    return new Promise((resolve) =>
        setTimeout(() => resolve({ data: amount }), 500)
    );
}

export const getPostsAPI = async () => {
    const res = await axios.get(`https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json`);
    return res;
};

export const getPostAPI = async (id) => {
    const res = await axios.get(
        `https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json/${id}`
    );
    return res;
};

export const updatePostAPI = async (payload) => {
    const res = await axios.put(
        `https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json/${payload.id}`,
        payload,
    );
    return res;
};
