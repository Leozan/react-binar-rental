//import module or any..
import { useNavigate } from 'react-router-dom';
import React, { useState, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getPosts, data } from "./Reducer";

//css for React
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';

//import bootstrap component
import { Button, Card, Form } from 'react-bootstrap';

//import css local
import '../../assets/style.css'

import {
    Grid,
    Container,
    Typography
} from "@mui/material";


const CarsPage = () => {
    // template
    const posts = useSelector(data);
    const dispatch = useDispatch();

    const [driverType, setDriverType] = useState('');
    const [datepickr, setDatepickr] = useState('');
    const [time, setTime] = useState('');
    const [capacity, setCapacity] = useState('');

    const [dataFiltered, setDataFiltered] = useState([])

    const filterType = (data) => {
        if (driverType === "true") {
            let newdata = data.filter((row) => row.available === true);
            return newdata;
        }
        if (driverType === "false") {
            let newdata = data.filter((row) => row.available === false);
            return newdata;
        }
        return data;
    }

    const filterDate = (data) => {
        if (datepickr === '') {
            return data;
        }

        let newDate = data.filter((row) => row.availableAt.slice(5, 7) === datepickr.slice(5, 7));
        console.log(`Input Date:\n ${datepickr} into ${datepickr.slice(5, 7)}`)
        return newDate;

    }

    const filterTime = (data) => {
        if (time === '') {
            return data;
        }
        let newTime = data.filter((row) => row.availableAt.slice(11, 13) <= time.slice(0, 2))
        console.log(`Input :\n ${time} into ${time.slice(0, 2)}`)
        return newTime;
    }

    const filterCapacity = (data) => {
        if (capacity === '') {
            return data
        }
        let newCapacity = data.filter((row) => row.capacity == capacity);
        return newCapacity;
    }


    const fetchData = () => {
        dispatch(getPosts());
    };

    useEffect(() => {
        fetchData();
    }, []);
    // end template

    useEffect(() => {
        let newVar = filterType(posts) //semua data ditampilin
        newVar = filterDate(newVar)
        newVar = filterTime(newVar)
        newVar = filterCapacity(newVar)
        setDataFiltered(newVar)
    }, [driverType, datepickr, time, capacity]);




    let navigateTo = useNavigate();
    function backToHome() {
        navigateTo('/')
    };
    function resetPage() {
        navigateTo('/cars')
    }

    console.log(posts)

    return (
        <div className='App'>
            {/* filter section */}
            <div className='filter-container'>
                <div className='filter-card'>
                    <Form>
                        <div className='row row-cols-4 filter-content'>
                            <div className='col-sm-3'>
                                <p className='text-left' >Tipe Driver</p>
                                <select className='form-select' onChange={(e) => setDriverType(e.target.value)} value={driverType}>
                                    <option selected disabled="disabled">Pilih Driver</option>
                                    <option value={true}>Dengan Supir</option>
                                    <option value={false}>Tanpa Supir (lepas kunci)</option>
                                </select>
                            </div>
                            <div className='col-sm-3'>
                                <p className='text-left'>Tanggal</p>
                                <input className='form-control' type='date' onChange={(e) => setDatepickr(e.target.value)} value={datepickr}></input>
                            </div>
                            <div className='col-sm-3'>
                                <p className='text-left'>Waktu</p>
                                <input className='form-control' type='time' onChange={(e) => setTime(e.target.value)} value={time}></input>
                            </div>
                            <div className='col-sm-3'>
                                <p className='text-left'>Penumpang</p>
                                <input className='form-control' type='number' onChange={(e) => setCapacity(e.target.value)} value={capacity}></input>
                            </div>
                        </div>
                    </Form>
                    <div className='button-container-filter'>
                        <div className='bcf-bottom'>
                            <Button className="btn-back" onClick={backToHome}>Back</Button>{' '}
                        </div>
                    </div>
                </div>
            </div>
            {/* show result section */}
            <div className='car-product-container'>
                <div className='product-body'>
                    <Container style={{ marginTop: '50px' }}>
                        <Grid container spacing={3}>
                            {dataFiltered.map((row, i) => (
                                <Grid key={i} item xs={3}>
                                    <Card className='rounded-sm height-flex' id='card-noEffect'>
                                        <div className="card-image-container">
                                            <img className="cic-img" src={row.image} alt={row.manufacture} />
                                        </div>
                                        <div className='ci-head'>
                                            <p className='ci-head-text text-left'>{row.manufacture} <span className='text-muted-sm' >({row.year})</span></p>
                                        </div>
                                        <div className="ci-body text-left">
                                            <p> Avail in Month: {row.availableAt.slice(5, 7)} at {row.availableAt.slice(11, 16)} </p>
                                            <p>Capacity: {row.capacity} Person</p>
                                            <p>Transmission: {row.transmission}</p>
                                            <p>Type: {row.type}</p>
                                            <p className='price text-right'><sup className='text-sup'>Price</sup> Rp. {row.rentPerDay}/Day</p>

                                            {/* <p>{row.availableAt} </p> */}
                                            {/* <p>{row.description}</p> */}

                                            <div className='btn-place-card'>
                                                <Button variant="success">Select</Button>{' '}
                                            </div>

                                        </div>
                                        {/* <Button variant="contained" color="success" onClick={() => handleClick(row?.id)}>Pilih Mobil</Button> */}

                                    </Card>
                                </Grid>
                            ))}
                            {posts.status === "loading" && <Typography>Loading</Typography>}
                        </Grid>
                    </Container>

                </div>
            </div>
            {/* Footer Section */}
            <div className='footer-container'>
                <div className='footer-content'>
                    <div className='footer-info'>
                        <p className='text-left'>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p className='text-left'>binarcarrental@gmail.com</p>
                        <p className='text-left'>081-233-334-808</p>
                    </div>
                    <div className='footer-info'>
                        <div className='fi-link'>
                            <ul>
                                <li className='text-left' ><a href="#">Home</a></li>
                                <li className='text-left' ><a href="#">Our Service</a></li>
                                <li className='text-left' ><a href="#">Why Us</a></li>
                                <li className='text-left' ><a href="#">Testimonial</a></li>
                                <li className='text-left' ><a href="#">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className='footer-info'>
                        <p className='text-left'>Connect with Us:</p>
                        <div className='fi-sm'>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-facebook-f' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-instagram' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-twitter' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-twitch' id='fa-custom02'></i>
                            </div>
                        </div>
                    </div>

                    <div className='footer-info'>
                        <p className='text-left'>Copyright Binar 2022</p>
                        <p className='my-heading text-left'>BinaRent</p>
                    </div>


                </div>
            </div>

        </div>
    )
}

export default CarsPage;