import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import postsReducer from "../features/posts/Reducer";

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        cars: postsReducer,
    },
});