//import module or any...
import React from 'react';
import { useNavigate } from 'react-router';
import '../../assets/fontawesome6/css/all.min.css'
import '../../assets/fontawesome6/css/fontawesome.min.css'

//css for React
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';

//css local
import '../../assets/style.css';
//import bootstrap component
import { Button, Card } from 'react-bootstrap';

//import resource
import carImage from '../../assets/img/img_car.png';
import personService from '../../assets/img/img_service.png'

function App() {
    let navigateToCar = useNavigate();
    function pageMove() {
        navigateToCar('/cars')
    }
    return (

        <div className='App'>
            {/* poster section */}
            <div className='poster'>
                <div className='poster-img'>
                    <img src={carImage} id="img01" alt='--'></img>
                </div>

                <div className='poster-content'>
                    <p className='poster-title text-left'>Sewa & Rental Mobil Terbaik di Kawasan (Lokasimu)</p>
                    <p className='poster-desc text-left'>Selamat Datang di Binar Car Rental.Kami menyediakan mobil kualitas terbaik dengan harga
                        terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    <div className='poster-bottom'>
                        <Button variant="success" id='btn-openFilter' onClick={pageMove}>Mulai Sewa Mobil</Button>
                    </div>
                </div>
            </div>
            {/* feature section */}
            <div className='feature'>
                <div className='feature-container'>
                    <div className='fc-img'>
                        <img src={personService} id="img02" alt='--'></img>
                    </div>

                    <div className='fc-desc'>
                        <p className='fcd-head'>Best Car Rental for any kind of trip in (Lokasimu)!</p>
                        <p className='fcd-desc'>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan
                            yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
                            meeting, dll.</p>
                        <div className='fc-option'>
                            <div className='fc-icon'>
                                <i className='fa-solid fa-check' id='fa-custom01'></i>
                            </div>
                            <p className='fc-text'>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                        </div>
                        <div className='fc-option'>
                            <div className='fc-icon'>
                                <i className='fa-solid fa-check' id='fa-custom01'></i>
                            </div>
                            <p className='fc-text'>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                        </div>
                        <div className='fc-option'>
                            <div className='fc-icon'>
                                <i className='fa-solid fa-check' id='fa-custom01'></i>
                            </div>
                            <p className='fc-text'>Sewa Mobil Jangka Panjang Bulanan</p>
                        </div>
                        <div className='fc-option'>
                            <div className='fc-icon'>
                                <i className='fa-solid fa-check' id='fa-custom01'></i>
                            </div>
                            <p className='fc-text'>Gratis Antar - Jemput Mobil di Bandara</p>
                        </div>
                        <div className='fc-option'>
                            <div className='fc-icon'>
                                <i className='fa-solid fa-check' id='fa-custom01'></i>
                            </div>
                            <p className='fc-text'>Layanan Airport Transfer / Drop In Out</p>
                        </div>
                    </div>
                </div>
            </div>

            {/* why section */}

            <div className='card-container'>
                <div className='cc-head'>
                    <p className='my-heading text-left'>Why Us?</p>
                    <p className='cch-desc text-left'>Mengapa harus pilih Binar Car Rental?</p>
                </div>

                <div className='row row-cols-1 row-cols-4 row-custom'>
                    {/* card-01 */}
                    <div className='col mb-4'>
                        <Card className='rounded-sm'>
                            <div className='card-icon-1 icon-center'>
                                <i className='fa-solid fa-thumbs-up' id='fa-custom02'></i>
                            </div>
                            <Card.Body>
                                <Card.Title className='text-left'>Mobil Lengkap</Card.Title>
                                <Card.Text className='text-left'>
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    {/* card-02 */}
                    <div className='col mb-4'>
                        <Card className='rounded-sm'>
                            <div className='card-icon-2 icon-center'>
                                <i className='fa-solid fa-tag' id='fa-custom02'></i>
                            </div>
                            <Card.Body>
                                <Card.Title className='text-left'>Mobil Lengkap</Card.Title>
                                <Card.Text className='text-left'>
                                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    {/* card-03 */}
                    <div className='col mb-4'>
                        <Card className='rounded-sm'>
                            <div className='card-icon-3 icon-center'>
                                <i className='fa-solid fa-clock' id='fa-custom02'></i>
                            </div>
                            <Card.Body>
                                <Card.Title className='text-left'>Layanan 24 Jam</Card.Title>
                                <Card.Text className='text-left'>
                                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir
                                    minggu
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    {/* card-04 */}
                    <div className='col mb-4'>
                        <Card className='rounded-sm'>
                            <div className='card-icon-4 icon-center'>
                                <i className='fa-solid fa-award' id='fa-custom02'></i>
                            </div>
                            <Card.Body>
                                <Card.Title className='text-left'>Sopir Profesional</Card.Title>
                                <Card.Text className='text-left'>
                                    Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>

                </div>

            </div>

            {/* FAQ section */}
            <div className='question'>
                <div className='qt-head'>
                    <p className='my-heading'>Frequently Asked Question</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
                {/* accordion-1 */}
                <div className='accordion' id='accordionExample'>
                    <div className='accordion-item'>
                        <h2 className='accordion-header'>
                            <Button className='accordion-button collapsed' data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" >Apa saja syarat yang dibutuhkan?</Button>
                        </h2>
                        <div className='accordion-collapse collapse' id="collapseOne" data-bs-parent="#accordionExample">
                            <div className='accordion-body text-left'>
                                <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse
                                plugin adds the appropriate classes that we use to style each element. These classes control the
                                overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this
                                with custom CSS or overriding our default variables. It's also worth noting that just about any HTML
                                can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
                {/* accordion-2 */}
                <div className='accordion my-pt-20' id="accordionExample">
                    <div className='accordion-item'>
                        <h2 className='accordion-header'>
                            <Button className='accordion-button collapsed' data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" >Berapa hari minimal sewa mobil lepas kunci?</Button>
                        </h2>
                        <div className='accordion-collapse collapse' id="collapseTwo" data-bs-parent="#accordionExample">
                            <div className='accordion-body text-left'>
                                <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse
                                plugin adds the appropriate classes that we use to style each element. These classes control the
                                overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this
                                with custom CSS or overriding our default variables. It's also worth noting that just about any HTML
                                can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
                {/* accordion-3 */}
                <div className='accordion my-pt-20' id='accordionExample'>
                    <div className='accordion-item'>
                        <h2 className='accordion-header'>
                            <Button className='accordion-button collapsed' data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" >Berapa hari sebelumnya sabaiknya booking sewa mobil?</Button>
                        </h2>
                        <div className='accordion-collapse collapse' id="collapseThree" data-bs-parent="#accordionExample">
                            <div className='accordion-body text-left'>
                                <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse
                                plugin adds the appropriate classes that we use to style each element. These classes control the
                                overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this
                                with custom CSS or overriding our default variables. It's also worth noting that just about any HTML
                                can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
                {/* accordion-4 */}
                <div className='accordion my-pt-20' id='accordionExample'>
                    <div className='accordion-item'>
                        <h2 className='accordion-header'>
                            <Button className='accordion-button collapsed' data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" >Apakah Ada biaya antar-jemput?</Button>
                        </h2>
                        <div className='accordion-collapse collapse' id="collapseFour" data-bs-parent="#accordionExample">
                            <div className='accordion-body text-left'>
                                <strong>This is the fourth item's accordion body.</strong> It is hidden by default, until the collapse
                                plugin adds the appropriate classes that we use to style each element. These classes control the
                                overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this
                                with custom CSS or overriding our default variables. It's also worth noting that just about any HTML
                                can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
                {/* accordion-5 */}
                <div className='accordion my-pt-20' id='accordionExample'>
                    <div className='accordion-item'>
                        <h2 className='accordion-header'>
                            <Button className='accordion-button collapsed' data-bs-toggle="collapse"
                                data-bs-target="#collapseFive" >Bagaimana jika terjadi kecelakaan?</Button>
                        </h2>
                        <div className='accordion-collapse collapse' id="collapseFive" data-bs-parent="#accordionExample">
                            <div className='accordion-body text-left'>
                                <strong>This is the fifth item's accordion body.</strong> It is hidden by default, until the
                                collapse
                                plugin adds the appropriate classes that we use to style each element. These classes control the
                                overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of
                                this
                                with custom CSS or overriding our default variables. It's also worth noting that just about any HTML
                                can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Footer Section */}

            <div className='footer-container'>
                <div className='footer-content'>
                    <div className='footer-info'>
                        <p className='text-left'>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p className='text-left'>binarcarrental@gmail.com</p>
                        <p className='text-left'>081-233-334-808</p>
                    </div>
                    <div className='footer-info'>
                        <div className='fi-link'>
                            <ul>
                                <li className='text-left' ><a href="#">Home</a></li>
                                <li className='text-left' ><a href="#">Our Service</a></li>
                                <li className='text-left' ><a href="#">Why Us</a></li>
                                <li className='text-left' ><a href="#">Testimonial</a></li>
                                <li className='text-left' ><a href="#">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className='footer-info'>
                        <p className='text-left'>Connect with Us:</p>
                        <div className='fi-sm'>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-facebook-f' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-instagram' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-twitter' id='fa-custom02'></i>
                            </div>
                            <div className='sm-icon'>
                                <i className='fa-brands fa-twitch' id='fa-custom02'></i>
                            </div>
                        </div>
                    </div>

                    <div className='footer-info'>
                        <p className='text-left'>Copyright Binar 2022</p>
                        <p className='my-heading text-left'>BinaRent</p>
                    </div>


                </div>
            </div>
        </div>
    );
}

export default App;