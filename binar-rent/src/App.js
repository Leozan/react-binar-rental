//import module or any...
import './App.css';

import { Routes, Route } from 'react-router-dom';
import CarPage from './features/posts/CarsPage';
import Home from './app/pages/Home'

//css for React
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';

//css local
import './assets/style.css';

//import bootstrap component
import { Navbar, Nav, Container, Button } from 'react-bootstrap';


function App() {
  const menu = [
    { path: "/", component: <Home /> },
    { path: "/cars", component: <CarPage /> }
  ]
  return (
    <div className="App">
      {/* navbar section */}
      <Navbar expand="lg" className='bg-parent'>
        <Container>
          <Navbar.Brand href="#home" className=''>BinaRent</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto navbar-right">
              <Nav.Link href="#home">Our Service</Nav.Link>
              <Nav.Link href="#link">Why Us</Nav.Link>
              <Nav.Link href="#link">Testimonial</Nav.Link>
              <Nav.Link href="#link">FAQ</Nav.Link>
              <Button variant="success" id="btn-regis">Register</Button>{' '}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <div class="App">
        <Routes>
          {menu.map((row, i) =>
            <Route exact path={row.path} element={row.component}></Route>
          )}
        </Routes>
      </div>






    </div>
  );
}

export default App;
